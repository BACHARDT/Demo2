open Triangle3D;;

type triangle_set_3D = triangle_3D list;;

let empty_ts_3D () = ([]: triangle_set_3D);;

let is_empty_ts_3D ts_3D =
	if ts_3D = (empty_ts_3D ()) then true
	else false
;;

let cons_ts_3D a (t: triangle_set_3D) = (a::t: triangle_set_3D);;

let car_ts_3D (t: triangle_set_3D) = match t with
	|[] -> failwith "car_ts_3D"
	|h::t -> (h:triangle_3D);;

let cdr_ts_3D (t: triangle_set_3D) = match t with
	|[] -> failwith "cd_ts_3D"
	|h::t -> (t: triangle_set_3D);;

let rec length_ts_3D (t: triangle_set_3D) =
	if is_empty_ts_3D t then 0
	else 1 + (length_ts_3D (cdr_ts_3D t));;

let concat_ts_3D (t1: triangle_set_3D) (t2: triangle_set_3D) = (t1@t2: triangle_set_3D);;



