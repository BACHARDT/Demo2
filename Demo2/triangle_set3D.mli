type triangle_set_3D = Triangle3D.triangle_3D list
val empty_ts_3D : unit -> triangle_set_3D
val is_empty_ts_3D : triangle_set_3D -> bool
val cons_ts_3D : Triangle3D.triangle_3D -> triangle_set_3D -> triangle_set_3D
val car_ts_3D : triangle_set_3D -> Triangle3D.triangle_3D
val cdr_ts_3D : triangle_set_3D -> triangle_set_3D
val length_ts_3D : triangle_set_3D -> int
val concat_ts_3D : triangle_set_3D -> triangle_set_3D -> triangle_set_3D
