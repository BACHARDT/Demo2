val size_x : int
val size_y : int
val radius : int
val init_draw : unit -> unit
val draw_points : Point_set.point_set -> unit
val draw_one_triangle : Triangle.triangle -> unit
val draw_triangles : Triangle_set.triangle_set -> unit
