(* Module de test de la seconde présentation. *)

open Point;;
open Point_set;;
open Triangle;;
open Triangle_set;;
open Drawing;;
open Delaunay;;
open Vector;;
open Point3D;;
open Point_set3D;;
open Triangle3D;;
open Triangle_set3D;;
open Plane;;
open Graphics;;
open Maths;;

print_string "#################\n";;
print_string "#################\n";;

print_string "Voici les tests sur la partie obligatoire.\nAppuyez sur entrée pour commencer.\n";;

read_line();;

print_string "Premier test du module obligatoire.\n";;

let points = (random 20 800 600) in delaunay_step_by_step points;;

print_string "Appuyez sur entrée.\n";;

read_line();;
close_graph();;

print_string "Second test du module obligatoire.\n";;

let points = random 100 800 600 in delaunay points 800 600;;

print_string "Appuyez sur entrée.\n";;

read_line();;
clear_graph();;

print_string "#################\n";;
print_string "#################\n";;

print_string "Voici les tests sur la partie des extensions.\nAppuyez sur entrée pour commencer.\n";;

read_line();;

print_string "Premier test des extensions.\n";;
print_string "Inversion d'une matrice.\n\n";;

try
	let m = create_c_by_c [0. ; 0. ; 0. ; 0.] 2 in let inverse_m = inverse_matrix m in ();
with
	| NonInvertible -> print_string "Matrice non inversible : Ok.\n"
;;

let m = create_c_by_c [0. ; 0. ; 0. ; 1. ; 0. ; 0. ; 1. ; 0. ; 0. ; -1. ; 0. ; 0. ; -1. ; 0. ; 0. ; 0.] 4 in inverse_matrix m;;

print_string "Matrice inversible : Ok.\n";;

print_string "Appuyez sur entrée.\n";;

read_line();;

print_string "Deuxième test des extensions.\n";;
print_string "Résolution d'un système linéaire.\n";;

let m = create_c_by_c [1. ; 1. ; 0. ; 1. ; 0. ; 1. ; 1. ; 0. ; 3.] 3 and b = [| 0. ; 4. ; 0. |] in solve_linear_system m b;;

print_newline();;
print_string "Résolution d'un système linéaire : Ok.\n";

print_string "Appuyez sur entrée.\n";;

read_line();;

print_string "Troisième test des extensions.\n";;
print_string "Projection sur un plan.\n\n";;

print_string "L'équation du plan est : 3x - 5y + 8z = 3";;

let o = create_3D 0. 0. (3./.8.);;
let n = create_vector 3. (-5.) 8.;;
let p = create_plane n o;;
let p1 = create_3D (1.) (1.) (5./.8.);;
let v1 = plan3D_2D p1 p;;
let p2 = create_3D (2.) (2.) (7./.8.);;
let v2 = plan3D_2D p2 p;;
let p3 = create_3D (-25./.4.) (25./.4.) (69./.8.);;
let v3 = plan3D_2D p3 p;;
let p4 = create_3D (-21./.4.) (29./.4.) (71./.8.);;
let v4 = plan3D_2D p4 p;;
print_string("\nPoints projetés sur :");;
print_string("\nPoint 1 :");;
print_float(get_x v1);;
print_string " ";;
print_float(get_y v1);;
print_string("\nPoint 2 :");;
print_float(get_x v2);;
print_string " ";;
print_float(get_y v2);;
print_string("\nPoint 3 :");;
print_float(get_x v3);;
print_string " ";;
print_float(get_y v3);;
print_string("\nPoint 4 :");;
print_float(get_x v4);;
print_string " ";;
print_float(get_y v4);;
print_string "\nProjection d'un rectangle sur un plan non trivial : Ok.\n";;

print_string "Appuyez sur entrée.\n";;

read_line();;

print_string "Quatrième test sur les extensions.\n";;
print_string "Changement de base.\n";;

let p = create_plane (create_vector 1. 2. 3.) (create_3D 2. 3. 4.) in orthonormal_base p;;

print_string "Création d'une base orthonormale à partir d'un plan et d'un point : Ok.\n";;

let p = create_plane (create_vector 1. 2. 3.) (create_3D 2. 3. 4.) in transfer_matrix p;;

print_string "Création d'une matrice de changerment de base : Ok\n";;

print_string "Présentation du menu terminée.\n Appuyez sur entrée pour quitter.\n";;

read_line();;

#quit;;



