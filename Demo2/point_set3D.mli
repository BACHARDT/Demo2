type point_set_3D = Point3D.point3D list
val empty_ps_3D : unit -> point_set_3D
val is_empty_ps_3D : point_set_3D -> bool
val cons_3D : point_set_3D -> Point3D.point3D -> point_set_3D
val cdr_ps_3D : point_set_3D -> point_set_3D
val car_ps_3D : point_set_3D -> Point3D.point3D
val random_3D : int -> int -> int -> int -> point_set_3D
