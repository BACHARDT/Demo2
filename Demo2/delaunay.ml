open Point;;
open Point_set;;
open Maths;;
open Triangle;;
open Triangle_set;;
open Drawing;;
open Graphics;;
open Unix;;

let in_circle triangle point=
	let a = get_p1 triangle
	and b = get_p2 triangle
	and c = get_p3 triangle in
	let ax = get_x a
	and ay = get_y a
	and bx = get_x b
	and by = get_y b
	and cx = get_x c
	and cy = get_y c
	and dx = get_x point
	and dy = get_y point in
	let xtab = [|ax;bx;cx;dx|] and ytab = [|ay;by;cy;dy|] in
	let aux l i = let x = xtab.(i) and y = ytab.(i) in
		x::y::(x*.x+.y*.y)::(1.)::l
	in let l = (aux (aux (aux (aux [] 3) 2) 1) 0) in
	let det = determinant (create_c_by_c l 4) in
	((det > 0.) == (ccw a b c));;

let rec remove_triangles triangles point =
	if (is_empty_ts triangles) then ((empty_ts ()), (empty_ts ()))
	else let tri = (car_ts triangles) in
		let to_remove, to_keep = remove_triangles (cdr_ts triangles) point in
		if (in_circle tri point) then
			((cons_ts tri to_remove), to_keep)
		else
			(to_remove, (cons_ts tri to_keep));;
			
let get_line p1 p2=
	if (get_x p1) < (get_x p2) then (p1,p2)
	else if (get_x p1) > (get_x p2) then (p2,p1)
	else if (get_y p1) > (get_y p2) then (p1,p2)
	else (p2,p1);;
			
let rec get_all_lines triangles =
	if (is_empty_ts triangles) then []
	else let a = (car_ts triangles) in
		let p1 = get_p1 a
		and p2 = get_p2 a
		and p3 = get_p3 a in
		let l1 = get_line p1 p2
		and l2 = get_line p2 p3
		and l3 = get_line p3 p1 in
		l1::l2::l3::(get_all_lines (cdr_ts triangles));;
	
let rec compute_occurence line l = match l with (* l = [ (line0,1) ; (line1,1) ] *)
	|[] -> [(line,1)]
	|((a,b)::t) when a = line -> (line,(b+1))::t
	|(h::t) -> h::(compute_occurence line t);; 
	
let rec get_correct_occurence occurence = match occurence with
	|[] -> []
	|((a,b)::t) when b = 1 -> a::(get_correct_occurence t)
	|(h::t) -> (get_correct_occurence t)
		
let border triangles =
	let all_lines = get_all_lines triangles in
	let rec aux lines occurence =
		if lines = [] then (get_correct_occurence occurence)
		else begin
			let line = (List.hd lines) in
			(aux (List.tl lines) (compute_occurence line occurence))
		end
	in aux all_lines [];;
	
let rec add_triangles frontier point triangles =
	match frontier with
	|[] -> triangles
	|(a,b)::t -> let tri = (create_triangle a b point) in
				cons_ts tri (add_triangles t point triangles);;
			
let rec add_point triangles point = 
	let to_remove, to_keep = remove_triangles triangles point in
	if (is_empty_ts to_remove) then triangles
	else let frontier = border to_remove in
	add_triangles frontier point to_keep;;

let delaunay points max_x max_y = 
	let x = float_of_int max_x
	and y = float_of_int max_y in
	let triangle1 = create_triangle (create 0. 0.) (create x 0.) (create 0. y) in
	let triangle2 = create_triangle (create x y) (create x 0.) (create 0. y) in
	let triangles = (cons_ts triangle1 (cons_ts triangle2 (empty_ts ()))) in
	init_draw ();
	set_color black;
	let rec test pointsR pointsN triangles =
		if pointsN = [] then triangles
		else begin
			let point1 = List.hd pointsN in
			let triangles1 = add_point triangles point1 in
			let points1 = (List.tl pointsN) in 
			test (point1::pointsR) points1 triangles1 end
	in draw_triangles (test [] points triangles);;

let delaunay_step_by_step points=
	let triangle1 = create_triangle (create 0. 0.) (create 800. 0.) (create 0. 600.) in
	let triangle2 = create_triangle (create 800. 600.) (create 800. 0.) (create 0. 600.) in
	let triangles = cons_ts triangle1 (cons_ts triangle2 (empty_ts())) in
	init_draw ();
	draw_points points;
	draw_triangles triangles;
	let rec test pointsR pointsN triangles =
		Unix.sleepf(0.1);
		if pointsN != [] then begin
			let point1 = List.hd pointsN in
			let triangles1 = add_point triangles point1 in
			let points1 = (List.tl pointsN) in
			clear_graph ();
			set_color black;
			draw_points points1;
			draw_triangles triangles1;
			set_color red;
			draw_points (point1::pointsR);
			test (point1::pointsR) points1 triangles1 end
	in test [] points triangles;;

