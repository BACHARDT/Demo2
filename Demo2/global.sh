


#!/bin/bash
ocamlc -i point.ml > point.mli
read d
ocamlc -c point.mli
ocamlc -c point.ml

ocamlc -i maths.ml > maths.mli
read d
ocamlc -c maths.mli
ocamlc -c maths.ml

ocamlc -i triangle.ml > triangle.mli
read d
ocamlc -c triangle.mli
ocamlc -c triangle.ml

ocamlc -i triangle_set.ml > triangle_set.mli
read d
ocamlc -c triangle_set.mli
ocamlc -c triangle_set.ml

ocamlc -i point_set.ml > point_set.mli
read d
ocamlc -c point_set.mli
ocamlc -c point_set.ml

ocamlc -i drawing.ml > drawing.mli
read d
ocamlc -c drawing.mli
ocamlc -c drawing.ml

ocamlc -i delaunay.ml > delaunay.mli
read d
ocamlc -c delaunay.mli
ocamlc -c delaunay.ml

ocamlc -i vector.ml > vector.mli
read d
ocamlc -c vector.mli
ocamlc -c vector.ml

ocamlc -i point3D.ml > point3D.mli
read d
ocamlc -c point3D.mli
ocamlc -c point3D.ml


ocamlc -i point_set3D.ml > point_set3D.mli
read d
ocamlc -c point_set3D.mli
ocamlc -c point_set3D.ml


ocamlc -i triangle3D.ml > triangle3D.mli
read d
ocamlc -c triangle3D.mli
ocamlc -c triangle3D.ml


ocamlc -i triangle_set3D.ml > triangle_set3D.mli
read d
ocamlc -c triangle_set3D.mli
ocamlc -c triangle_set3D.ml


ocamlc -i plane.ml > plane.mli
read d 
ocamlc -c plane.mli
ocamlc -c plane.ml



echo "La compilation est terminée."
