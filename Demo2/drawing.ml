open Point;;
open Point_set;;
open Triangle_set;;
open Triangle;;
open Graphics;;

let size_x = 800;;
let size_y = 600;;
let radius = 2;;

let init_draw () =
	open_graph "";
	set_window_title "Triangulation de Delaunay !";
	resize_window size_x size_y
;;

let rec draw_points point_set =
	if is_empty_ps point_set then ()
	else begin
		let p = car_ps point_set in
		let x = int_of_float (get_x p) and y = int_of_float (get_y p) in
		for i = 0 to radius do
			draw_circle x y i
		done;
		plot x y;
		draw_points (cdr_ps point_set)
	end
;;

let draw_one_triangle triangle =
	let p1 = get_p1 triangle
	and p2 = get_p2 triangle
	and p3 = get_p3 triangle in
	let x1 = int_of_float (get_x p1)
	and y1 = int_of_float (get_y p1)
	and x2 = int_of_float (get_x p2)
	and y2 = int_of_float (get_y p2)
	and x3 = int_of_float (get_x p3)
	and y3 = int_of_float (get_y p3) in
	moveto x1 y1;
	lineto x2 y2;
	lineto x3 y3;
	lineto x1 y1
;;

let rec draw_triangles triangle_set =
	if is_empty_ts triangle_set then ()
	else begin
		draw_one_triangle (car_ts triangle_set);
		draw_triangles (cdr_ts triangle_set)
	end
;;
