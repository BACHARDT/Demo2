type matrix = float array array
val empty_m : int -> matrix
val set_coeff : float -> matrix -> int -> int -> unit
val length : matrix -> int
val get_coeff : matrix -> int -> int -> float
val create_c_by_c : float list -> int -> matrix
exception Exist
exception DoesntExist
val exist_pivot_j : matrix -> int -> bool
exception Found of int
val get_pivot_j : matrix -> int -> int
val swap_lines : matrix -> int -> int -> unit
val substract_to_line : matrix -> int -> int -> float -> unit
exception NonInvertible
val determinant : matrix -> float
val create_identity : int -> matrix
val inverse_matrix : matrix -> matrix
val solve_linear_system : matrix -> float array -> float array
