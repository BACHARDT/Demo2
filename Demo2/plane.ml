open Point;;
open Point3D;;
open Maths;;
open Triangle;;
open Triangle_set;;
open Vector;;
open Triangle3D;;
open Triangle_set3D;;

type plane = {normal : vector ; center : point3D};;

let get_normal p = p.normal
;;

let get_center p = p.center
;;

let create_plane (vec: vector) (p:point3D) =
  {normal = vec ; center = p}
;;

let get_vector_in_plane plane =
  let o = get_center plane and n = get_normal plane in
let a = get_a n and b = get_b n and c = get_c n
and x = get_x_3D o and y = get_y_3D o and z = get_z_3D o in
  match a, b, c with
  |0.,0.,0. -> failwith "get_vector_in_plane"
  |_,_,_ when a <> 0.-> (create_vector ( -. ((b *. (1. -. y)) +. (c *. (1. -. z))) /. a)  (1. -. y) (1. -. z))
  |_,_,_ when b <> 0.-> (create_vector (1. -. x) (-. ((a *. (1. -. x)) +. (c *. (1. -. z)))/. b)  (1. -. z))
  |_,_,_ when c <> 0.-> (create_vector (1. -. x) (1. -. y) ( -. ((a *. (1. -. x)) +. (b *. (1. -. y))) /. c))
  |_,_,_ -> failwith "pattern-matching exhaustive now"
;;

let normalize vector =
  let a = get_a vector
  and b = get_b vector
  and c = get_c vector in
  if ((a == 0.) && (b == 0.) && (c == 0.)) then failwith "normalize"
  else let norm = sqrt (a**2. +. b**2. +. c**2.) in
    create_vector (a/.norm) (b/.norm) (c/.norm) ;;

let orthonormal_base plane =
  let bz = get_normal plane in
  let bx = get_vector_in_plane plane in
  let by = cross_product bz bx in
  ((normalize bx), (normalize by), (normalize bz));;

let transfer_matrix plane = 
  let (bx,by,bz) = orthonormal_base plane in
  (create_c_by_c [get_a bx; get_a by; get_a bz;
		  get_b bx; get_b by; get_b bz;
		  get_c bx; get_c by; get_c bz] 3);;

let matrix_product_3_1 m v =
  let x = (get_coeff m 0 0)*.(get_a v) +. (get_coeff m 0 1)*.(get_b v) +. (get_coeff m 0 2)*.(get_c v)
  and y = (get_coeff m 1 0)*.(get_a v) +. (get_coeff m 1 1)*.(get_b v) +. (get_coeff m 1 2)*.(get_c v)
  and z = (get_coeff m 2 0)*.(get_a v) +. (get_coeff m 2 1)*.(get_b v) +. (get_coeff m 2 2)*.(get_c v)
  in create_vector x y z;;

let plan3D_2D point3D plane =
  let o = get_center plane in
  let v = create_vector (get_x_3D point3D -. get_x_3D o) (get_y_3D point3D -. get_y_3D o) (get_z_3D point3D -. get_z_3D o) in
  let m = transfer_matrix plane in
  let v2 = matrix_product_3_1 m v in
  (create ((get_a v2) +. get_x_3D o) ((get_b v2) +. get_y_3D o));;
  
let proj_triangle_plan_2D t3D plane =
  let p1x = plan3D_2D (get_p1_3D t3D) plane
  and p2x = plan3D_2D (get_p2_3D t3D) plane
  and p3x = plan3D_2D (get_p3_3D t3D) plane
  in create_triangle p1x p2x p3x;;
  
let rec proj_triangle_set_plan_2D ts3D plane =
  if is_empty_ts_3D ts3D then (empty_ts ())
  else cons_ts (proj_triangle_plan_2D (car_ts_3D ts3D) plane) (proj_triangle_set_plan_2D (cdr_ts_3D ts3D) plane);;
  
let proj_point_3D_plan p3D plan =
  let a = get_a (get_normal plan) and b = get_b (get_normal plan) and c = get_c (get_normal plan) 
  and x0 = get_x_3D (get_center plan) and y0 = get_y_3D (get_center plan) and z0 = get_z_3D (get_center plan) in
  let m = create_c_by_c [1.; 0.; 0.; a; 0.; 1.; 0.; b; 0.; 0.; 1.; c; a; b; c; 0.] 4
  and b = [|get_x_3D p3D; get_y_3D p3D; get_z_3D p3D; a *. x0 +. b *. y0 +. c *. z0|] in
  let sol = solve_linear_system m b in
  let proj_p3D_on_plan = create_3D sol.(0) sol.(1) sol.(2) in proj_p3D_on_plan
;;

let proj_triangle_3D_plan t3D plan =
  let new_p1 = proj_point_3D_plan (get_p1_3D t3D) plan
  and new_p2 = proj_point_3D_plan (get_p2_3D t3D) plan
  and new_p3 = proj_point_3D_plan (get_p3_3D t3D) plan in
  let init_color = 0 in
  let proj_t3D_on_plan = create_triangle_3D new_p1 new_p2 new_p3 init_color
  in proj_t3D_on_plan;;

let proj_triangle_set_3D_plan ts3D plan =
  let rec aux ts3D plan proj_ts3D =
    if is_empty_ts_3D ts3D then proj_ts3D
    else
      begin
        let t3D = car_ts_3D ts3D in
        let proj_t3D = proj_triangle_3D_plan t3D plan in
        let new_proj_ts3D = cons_ts_3D proj_t3D proj_ts3D in
        aux (cdr_ts_3D ts3D) plan new_proj_ts3D
      end
  in
  aux ts3D plan (empty_ts_3D())
;;
