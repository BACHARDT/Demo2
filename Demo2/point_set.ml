open Point;;
open Random;;

self_init();;

type point_set = point list;;

let empty_ps ()= ([]: point_set);;

let is_empty_ps point_set =
	if point_set = (empty_ps ()) then true
	else false
;;

let cons (point_set: point_set) point = (point::point_set: point_set)
;;

let cdr_ps (point_set: point_set) =
	if is_empty_ps point_set then failwith "cdr_sp"
	else (List.tl point_set: point_set)
;;

let car_ps (point_set: point_set) =
	if is_empty_ps point_set then failwith "car_sp"
	else List.hd point_set
;;


let rec random nb max_x max_y = match nb with
	| 0 -> empty_ps ()
	| _ -> let x = (Random.float (float_of_int max_x)) and y = (Random.float (float_of_int max_y))
			in (create x y)::(random (nb - 1) max_x max_y)
;;
