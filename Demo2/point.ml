type point = {x: float ; y: float}
;;

let get_x p = p.x
;;

let get_y p = p.y
;;

let create i j = {x = i; y = j}
;;
