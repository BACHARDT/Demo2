type triangle_3D = {
  p1 : Point3D.point3D;
  p2 : Point3D.point3D;
  p3 : Point3D.point3D;
  color : int;
}
val get_p1_3D : triangle_3D -> Point3D.point3D
val get_p2_3D : triangle_3D -> Point3D.point3D
val get_p3_3D : triangle_3D -> Point3D.point3D
val get_color_3D : triangle_3D -> int
val create_triangle_3D :
  Point3D.point3D -> Point3D.point3D -> Point3D.point3D -> int -> triangle_3D
