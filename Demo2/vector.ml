type vector = {a : float ; b : float ; c : float};;

let get_a vec = vec.a
;;

let get_b vec = vec.b
;;

let get_c vec = vec.c
;;

let create_vector x y z =
	{a = x ; b = y ; c = z}
;;

let cross_product v1 v2 =
	let a1 = get_a v1 and a2 = get_a v2
	and b1 = get_b v1 and b2 = get_b v2
	and c1 = get_c v1 and c2 = get_c v2 in
	let new_a = b1*.c2 -. b2*.c1
	and new_b = c1*.a2 -. c2*.a1
	and new_c = a1*.b2 -. a2*.b1 in
	create_vector new_a new_b new_c
;;
