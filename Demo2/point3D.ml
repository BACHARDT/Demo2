type point3D = {x: float ; y: float; z : float}
;;

let get_x_3D p = p.x
;;

let get_y_3D p = p.y
;;

let get_z_3D p = p.z
;;

let create_3D i j k = {x = i; y = j; z = k}
;;
