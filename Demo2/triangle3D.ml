open Point3D;;

type triangle_3D = {p1 : point3D ; p2 : point3D ; p3 : point3D ; color : int}
;;

let get_p1_3D triangle_3D = triangle_3D.p1
;;

let get_p2_3D triangle_3D = triangle_3D.p2
;;

let get_p3_3D triangle_3D = triangle_3D.p3
;;

let get_color_3D triangle_3D = triangle_3D.color
;;

let create_triangle_3D point1 point2 point3 color_to_set =
	{p1 = point1 ; p2 = point2 ; p3 = point3 ; color = color_to_set}
;;
