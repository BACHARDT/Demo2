type matrix = float array array;;

let empty_m length = let m = Array.make_matrix length length 0. in (m: matrix);;

let set_coeff a_ij (m: matrix) i j = m.(i).(j) <- a_ij;;

let length (m: matrix) = Array.length m;;

let get_coeff (m: matrix) i j = m.(i).(j);;

let create_c_by_c list_coeff length =
  let rec fill_m m list_coeff i j=
    if j = length then ()
    else if i = length then fill_m m list_coeff 0 (j+1)
    else begin
      set_coeff (List.hd list_coeff) m i j;
      fill_m m (List.tl list_coeff) (i+1) j
    end
  in
  let m = empty_m length in
  fill_m m list_coeff 0 0;
  m;;

exception Exist;;
exception DoesntExist;;

let exist_pivot_j m j =
  let rec aux_exist m j i n =
    if i = n then false
    else if (get_coeff m i j) <> 0. then true
    else aux_exist m j (i+1) n;
  in
  try
    aux_exist m j j (length m)
  with
  |Exist -> true
  |DoesntExist -> false
;;

exception Found of int;;

let get_pivot_j (m: matrix) j =
  let rec aux_get_pivot m j i =
    if get_coeff m i j <> 0. then raise (Found i);
    aux_get_pivot m j (i+1);

  in
  try
    aux_get_pivot m j j
  with
  |Found i -> i
;;

let swap_lines m k j =
  let n = (length m) - 1 in
  for i = 0 to n do
    let a_ki = get_coeff m k i in (set_coeff (get_coeff m j i) m k i; set_coeff a_ki m j i);
  done
;;

let substract_to_line m i i0 coeff =
  let n = (length m) - 1 in
  for j = 0 to n do
    set_coeff (get_coeff m i j -. (coeff *. (get_coeff m i0 j))) m i j;
  done
;;

exception NonInvertible;;

let rec determinant m =
  let det = ref 1. in
  let n = (length m) - 1 in
  try
    for j = 0 to n-1 do
      if exist_pivot_j m j = false then raise NonInvertible;
      let k = get_pivot_j m j in
      (if k <> j then (swap_lines m k j; det := -1. *. !det));
      det := !det *. get_coeff m j j;
      for i = j+1 to n do
        let to_substract = ((get_coeff m i j) /. (get_coeff m j j)) in
        substract_to_line m i j to_substract;
      done
    done;
    det := !det *. m.(n).(n);
    !det
  with
  |NonInvertible -> 0.
;;


let create_identity n =
  let rec identity_list n l =
    if List.length l == (n*n) then l
    else if (List.length l mod n) == ((List.length l)/ n) then identity_list n (1.::l)
    else identity_list n (0.::l)
  in
  let l = identity_list n [] in
  let identity = create_c_by_c l n in identity
;;


let inverse_matrix a =
  let n = (length a) in
  let inverse_a = create_identity n in
  for j = 0 to n-1 do
    if exist_pivot_j a j = false then raise NonInvertible;
    let k = get_pivot_j a j in
    (if k <> j then (swap_lines a k j; swap_lines inverse_a k j));
    for i = 0 to n-1 do
      if i <> j then
        begin
          let to_substract = (get_coeff a i j) /. (get_coeff a j j) in
          (substract_to_line a i j to_substract; substract_to_line inverse_a i j to_substract)
        end
      else ()
    done;
  done;
  (* normalisation *)
  for i = 0 to n-1 do
    for j = 0 to n-1 do
      let new_coeff = (get_coeff inverse_a i j) /. (get_coeff a i i) in
      set_coeff new_coeff inverse_a i j;
    done;
    set_coeff 1. a i i;
  done;
  inverse_a;;

let solve_linear_system a b =
  let n = (length a) in
  let inverse_a = inverse_matrix a in
  let solution = Array.make n 0. in
  for i = 0 to n-1 do
    for j = 0 to n-1 do
      solution.(i) <- solution.(i) +. (get_coeff inverse_a i j) *. b.(j);
    done;
    print_float(solution.(i));
  done;
  solution;;
