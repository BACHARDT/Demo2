open Point3D;;
open Random;;

Random.self_init();;

type point_set_3D = point3D list;;

let empty_ps_3D ()= ([]: point_set_3D);;

let is_empty_ps_3D point_set =
	if point_set = (empty_ps_3D ()) then true
	else false
;;

let cons_3D (point_set_3D: point_set_3D) point_3D = (point_3D::point_set_3D: point_set_3D)
;;

let cdr_ps_3D (point_set_3D: point_set_3D) =
	if is_empty_ps_3D point_set_3D then failwith "cdr_sp_3D"
	else (List.tl point_set_3D: point_set_3D)
;;

let car_ps_3D (point_set_3D: point_set_3D) =
	if is_empty_ps_3D point_set_3D then failwith "car_sp_3D"
	else List.hd point_set_3D
;;


let rec random_3D nb max_x max_y max_z = match nb with
	| 0 -> empty_ps_3D ()
	| _ -> let x = (Random.float (float_of_int max_x)) and y = (Random.float (float_of_int max_y)) and z = (Random.float (float_of_int max_z))
			in (create_3D x y z)::(random_3D (nb - 1) max_x max_y max_z)
;;
