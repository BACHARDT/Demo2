type plane = { normal : Vector.vector; center : Point3D.point3D; }
val get_normal : plane -> Vector.vector
val get_center : plane -> Point3D.point3D
val create_plane : Vector.vector -> Point3D.point3D -> plane
val get_vector_in_plane : plane -> Vector.vector
val normalize : Vector.vector -> Vector.vector
val orthonormal_base : plane -> Vector.vector * Vector.vector * Vector.vector
val transfer_matrix : plane -> Maths.matrix
val matrix_product_3_1 : Maths.matrix -> Vector.vector -> Vector.vector
val plan3D_2D : Point3D.point3D -> plane -> Point.point
val proj_triangle_plan_2D :
  Triangle3D.triangle_3D -> plane -> Triangle.triangle
val proj_triangle_set_plan_2D :
  Triangle_set3D.triangle_set_3D -> plane -> Triangle_set.triangle_set
val proj_point_3D_plan : Point3D.point3D -> plane -> Point3D.point3D
val proj_triangle_3D_plan :
  Triangle3D.triangle_3D -> plane -> Triangle3D.triangle_3D
val proj_triangle_set_3D_plan :
  Triangle_set3D.triangle_set_3D -> plane -> Triangle_set3D.triangle_set_3D
