type vector = { a : float; b : float; c : float; }
val get_a : vector -> float
val get_b : vector -> float
val get_c : vector -> float
val create_vector : float -> float -> float -> vector
val cross_product : vector -> vector -> vector
