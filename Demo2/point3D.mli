type point3D = { x : float; y : float; z : float; }
val get_x_3D : point3D -> float
val get_y_3D : point3D -> float
val get_z_3D : point3D -> float
val create_3D : float -> float -> float -> point3D
