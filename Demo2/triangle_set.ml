open Triangle;;

type triangle_set = triangle list;;

let empty_ts () = ([]: triangle_set);;

let is_empty_ts t =
	if t == (empty_ts ()) then true
	else false;;

let cons_ts a (t: triangle_set) = (a::t: triangle_set);;

let car_ts (t: triangle_set) = match t with
	|[] -> failwith "car"
	|h::t -> h;;

let cdr_ts (t: triangle_set) = match t with
	|[] -> failwith "cdr"
	|h::t -> (t: triangle_set);;

let rec length_ts (t: triangle_set) =
	if is_empty_ts t then 0
	else 1 + (length_ts (cdr_ts t));;

let concat_ts (t1: triangle_set) (t2: triangle_set) = (t1@t2: triangle_set);;
