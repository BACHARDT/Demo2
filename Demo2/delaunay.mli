val in_circle : Triangle.triangle -> Point.point -> bool
val remove_triangles :
  Triangle_set.triangle_set ->
  Point.point -> Triangle_set.triangle_set * Triangle_set.triangle_set
val get_line : Point.point -> Point.point -> Point.point * Point.point
val get_all_lines :
  Triangle_set.triangle_set -> (Point.point * Point.point) list
val compute_occurence : 'a -> ('a * int) list -> ('a * int) list
val get_correct_occurence : ('a * int) list -> 'a list
val border : Triangle_set.triangle_set -> (Point.point * Point.point) list
val add_triangles :
  (Point.point * Point.point) list ->
  Point.point -> Triangle_set.triangle_set -> Triangle_set.triangle_set
val add_point :
  Triangle_set.triangle_set -> Point.point -> Triangle_set.triangle_set
val delaunay : Point.point list -> int -> int -> unit
val delaunay_step_by_step : Point_set.point_set -> unit
